# OpenML dataset: twonorm

https://www.openml.org/d/1507

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Michael Revow     
**Source**: http://www.cs.toronto.edu/~delve/data/twonorm/desc.html  
**Please cite**:     

* Twonorm dataset

This is an implementation of Leo Breiman's twonorm example[1]. It is a 20 dimensional, 2 class classification example. Each class is drawn from a multivariate normal distribution with unit variance. Class 1 has mean (a,a,..a) while Class 2 has mean (-a,-a,..-a). Where a = 2/sqrt(20). Breiman reports the theoretical expected misclassification rate as 2.3%. He used 300 training examples with CART and found an error of 22.1%.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1507) of an [OpenML dataset](https://www.openml.org/d/1507). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1507/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1507/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1507/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

